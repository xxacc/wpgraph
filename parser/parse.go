package parser

import (
	"log"
	"os"

	"github.com/dustin/go-wikiparse"

	"gitlab.com/xxacc/wpgraph/models"
)

func ReadPages(paths ...string) (pages []models.Page, err error) {
	pageC := make(chan models.Page)
	errC := make(chan error)
	go func() {
		err := StreamPages(pageC, paths...)
		for page := range pageC {
			pages = append(pages, page)
		}
		if err != nil {
			errC <- err
		} else {
			close(errC)
		}
	}()
	return pages, <-errC
}

func StreamPages(pageC chan<- models.Page, paths ...string) (err error) {
	for _, path := range paths {
		log.Print("Parsing ", path)
		f, err := os.Open(path)
		if err != nil {
			close(pageC)
			return err
		}
		p, err := wikiparse.NewParser(f)
		if err != nil {
			close(pageC)
			return err
		}
		for err == nil {
			var page *wikiparse.Page
			page, err = p.Next()
			if err == nil {
				pageC <- createPage(page)
			}
		}
	}
	close(pageC)
	return err
}

func createPage(p *wikiparse.Page) models.Page {
	return models.Page{
		ID:    models.ID(p.ID),
		NS:    models.NS{ID: models.ID(p.Ns)},
		Title: p.Title,
	}
}
