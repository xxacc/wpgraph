# The name of the project
NAME := $(shell basename $(CURDIR))

REPO  := gitlab.com/xxacc/wpgraph

# The architecture to build for
GOARCH ?= amd64

ALL_GOARCH := amd64

# This version-strategy uses git tags to set the version string
VERSION ?= $(shell git describe | grep -oE '[0-9]*\.[0-9]*\.[0-9]*-?[0-9]*')

# SHORT_VERSION does not include the commit number, used by packaging
SHORT_VERSION ?= $(shell git describe | grep -oE '[0-9]*\.[0-9]*\.[0-9]*')

COMMIT ?= $(shell git rev-parse --short --verify HEAD)

DIRS := $(addprefix bin/, $(GOARCH))

# Go related variables
GO := $(shell which go)
LINTTOOL := $(BIN_DIR)/revive

# List of binaries to build
BINS := $(shell basename $(CURDIR))

# List of packages to include in the binaries
PKGS := $(shell go list ./...)

# If you want to build all binaries, see the 'all-build' rule.
# If you want to build all containers, see the 'all-container' rule.
# If you want to build AND push all containers, see the 'all-push' rule.
all: lint test build

.PHONY: gen
gen:
	@$(GO) generate -v $(PKGS)

build-%:
	@$(MAKE) --no-print-directory ARCH=$* build

all-build: $(addprefix build-, $(ALL_GOARCH))

build: $(addprefix bin/$(GOARCH)/, $(BINS))

$(addprefix bin/$(GOARCH)/, $(NAME)): $(shell find . -name '*.go' -print)
	@echo "building: $@"
	CGO_ENABLED=0 \
	$(GO) build -v \
	-o $@ \
	-installsuffix "static" \
	-ldflags "-X main.Version=${VERSION} -X main.Commit=${COMMIT}" \
	.

version:
	@echo $(VERSION)
	@echo $(COMMIT)

dirs: $(DIRS)
$(DIRS):
	mkdir -p $@
	ls $@

.PHONY: test
test: build
	@$(GO) test -cover -timeout 30s -tags acceptance $(PKGS)

.PHONY: lint
lint: CGO_ENABLED := 0
lint: build
	@gofmt -l ./cmd ./pkg
	@revive -config=revive.toml -formatter=stylish $(PKGS)
	@$(GO) vet $(PKGS)

.PHONY: clean
clean:
	rm -rf .go bin gen kub-gen

.PHONY: image
image:
	docker build \
		-t $(REPO):$(VERSION) \
		--build-arg=VERSION=$(VERSION) \
		.
