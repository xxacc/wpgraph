package models

import "fmt"

type NS struct {
	ID   ID
	Name string
}

func (ns NS) String() string {
	return fmt.Sprint(ns.Name)
}
