package models

import (
	"fmt"
	"path"
	"strings"
	"time"

	"github.com/metal3d/go-slugify"
)

type Page struct {
	ID       ID
	NS       NS
	Site     Site
	Title    string
	Date     time.Time
	Children []string
}

func (p Page) String() string {
	return fmt.Sprintf("%v %v %v", p.ID, p.Title, p.NS)
}

func (p Page) Link() string {
	return path.Join(p.Site.Host, p.NS.Name, strings.Replace(slugify.Marshal(p.Title), "-", "_", -1))
}
