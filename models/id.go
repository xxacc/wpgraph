package models

import "fmt"

type ID int

func (id ID) String() string {
	return fmt.Sprint(int(id))
}
